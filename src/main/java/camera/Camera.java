package camera;

import blocks.Block;
import renderer.BitMap;
import renderer.Frame;
import renderer.GraphicRenderer;
import renderer.Renderable;
import world.World;

import java.util.ArrayList;

public class Camera {

    public int offsetX = 0;
    public int offsetY = 0;

    public int width = 0;
    public int height = 0;

    public GraphicRenderer renderer;

    public World world;

    public Camera(GraphicRenderer renderer) {
        this.renderer = renderer;
    }

    public Camera(int width, int height) {
        this(new GraphicRenderer());
        this.width = width;
        this.height = height;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Frame watch() {
        ArrayList<Renderable> blocks = this.world.getBlocksInRange(offsetX, offsetY, offsetX + width, offsetY + height);

        Frame frame = Frame.initialize(height, width);

        for (Renderable block : blocks) {
            int block_x = block.getX();
            int block_y = block.getY();
            int block_width = block.getWidth();
            int block_height = block.getHeight();

            int block_from_x = 0;
            int block_from_y = 0;
            int block_to_x = block_width;
            int block_to_y = block_height;

            if(block_x <= offsetX) {
                block_from_x = offsetX - block_x;
            }

            if(block_y <= offsetY) {
                block_from_y = offsetY - block_y;
            }

            if(block_x + block_width > offsetX + width) {
                block_to_x = -(block_x - offsetX - width + 1);
            }

            if(block_y + block_height > offsetY + height) {
                block_to_y = -(block_y - offsetY - height + 1);
            }

            //System.out.println(block_from_x + ":" + block_from_y + " - " + block_to_x + ":" + block_to_y);

            int i_y = 0;
            for (Character[] row : block.getBitmap().getMap()) {
                int i_x = 0;
                if(i_y < block_from_y || i_y > block_to_y) {
                    i_y++;
                    continue;
                }
                for(Character px : row) {
                    if(i_x < block_from_x || i_x >= block_to_x) {
                        i_x++;
                        continue;
                    }
                    frame.setPixel(i_y + block_y - offsetY, i_x + block_x - offsetX, px);
                    i_x++;
                }
                i_y++;
            }

        }

        return frame;

    }

}
