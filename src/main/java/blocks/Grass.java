package blocks;

import renderer.BitMap;

public class Grass extends Block {
    private Grass(int x, int y, Character[][] characters) {
        super(x, y, 8, 4, new BitMap(characters));
    }

    public static Grass build(int x, int y) {
        Character[][] characters = {
                {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
        };
        return new Grass(x, y, characters);
    }
}
