package blocks;

import renderer.BitMap;

public class DirtTopRight extends Block {
    private DirtTopRight(int x, int y, Character[][] characters) {
        super(x, y, 8, 4, new BitMap(characters));
    }

    public static DirtTopRight build(int x, int y) {
        Character[][] characters = {
                {'_', '_', '_', '_', '_', '_', '_', '_'},
                {' ', ' ', ' ', ' ', ' ', ' ', 'o', '|'},
                {' ', ' ', 'o', ' ', ' ', ' ', ' ', '|'},
                {'o', ' ', ' ', ' ', 'o', ' ', ' ', '|'},
        };
        return new DirtTopRight(x, y, characters);
    }
}
