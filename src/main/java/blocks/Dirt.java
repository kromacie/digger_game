package blocks;

import renderer.BitMap;

public class Dirt extends Block {
    private Dirt(int x, int y, Character[][] characters) {
        super(x, y, 8, 4, new BitMap(characters));
    }

    public static Dirt build(int x, int y) {
        Character[][] characters = {
                {' ', 'o', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' ', ' ', ' ', 'o', ' '},
                {' ', ' ', 'o', ' ', ' ', ' ', ' ', ' '},
                {'o', ' ', ' ', ' ', 'o', ' ', ' ', ' '},
        };
        return new Dirt(x, y, characters);
    }
}
