package blocks;

import renderer.BitMap;

public class DirtTop extends Block {
    private DirtTop(int x, int y, Character[][] characters) {
        super(x, y, 8, 4, new BitMap(characters));
    }

    public static DirtTop build(int x, int y) {
        Character[][] characters = {
                {'_', '_', '_', '_', '_', '_', '_', '_'},
                {' ', ' ', ' ', ' ', ' ', ' ', 'o', ' '},
                {' ', ' ', 'o', ' ', ' ', ' ', ' ', ' '},
                {'o', ' ', ' ', ' ', 'o', ' ', ' ', ' '},
        };
        return new DirtTop(x, y, characters);
    }
}
