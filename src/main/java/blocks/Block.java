package blocks;

import renderer.BitMap;
import renderer.Renderable;

public class Block implements Renderable {
    private int x;
    private int y;

    private int width;
    private int height;

    BitMap map;

    Block(int x, int y, int width, int height, BitMap map) {
        this.map = map;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    public BitMap getBitmap() {
        return map;
    }

}
