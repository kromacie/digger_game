package blocks;

import renderer.BitMap;

public class DirtTopLeft extends Block {
    private DirtTopLeft(int x, int y, Character[][] characters) {
        super(x, y, 8, 4, new BitMap(characters));
    }

    public static DirtTopLeft build(int x, int y) {
        Character[][] characters = {
                {'_', '_', '_', '_', '_', '_', '_', '_'},
                {'|', ' ', ' ', ' ', ' ', ' ', 'o', ' '},
                {'|', ' ', 'o', ' ', ' ', ' ', ' ', ' '},
                {'|', ' ', ' ', ' ', 'o', ' ', ' ', ' '},
        };
        return new DirtTopLeft(x, y, characters);
    }
}
