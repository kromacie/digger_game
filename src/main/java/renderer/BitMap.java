package renderer;

public class BitMap {
    private Character[][] map;

    public BitMap(Character[][] map) {
        this.map = map;
    }

    public Character[][] getMap() {
        return map;
    }

    public void setMap(Character[][] map) {
        this.map = map;
    }

    public void setXY(int x, int y, char c) {
        this.map[y][x] = c;
    }
}
