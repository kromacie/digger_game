package renderer;

public class Frame {
    private BitMap bitMap;

    private Frame(BitMap bitMap) {
        this.bitMap = bitMap;
    }

    public static Frame initialize(int sizeY, int sizeX) {

        Character[][] chars = new Character[sizeY][sizeX];

        for(int y = 0; y < sizeY; y++) {
            for(int x = 0; x < sizeX; x++) {
                chars[y][x] = ' ';
            }
        }
        return new Frame(
                new BitMap(chars)
        );
    }

    public void setPixel(int y, int x, char c) {
        this.bitMap.setXY(x, y, c);
    }

    public BitMap getBitMap() {
        return bitMap;
    }

    public void display() {
        Character[][] bitmap = this.getBitMap().getMap();

        for (Character[] row : bitmap) {
            for (Character field : row) {
                System.out.print(field);
            }
            System.out.println();
        }
    }
}
