package renderer;

import java.io.IOException;

public class GraphicRenderer {

    public void render(Frame frame) {
        this.clear();
        Character[][] bitmap = frame.getBitMap().getMap();

        for (Character[] row : bitmap) {
            for (Character field : row) {
                System.out.print(field);
            }
            System.out.println();
        }
    }

    public void clear() {
        final String os = System.getProperty("os.name");
        if (os.contains("Windows")) {
            try {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                Runtime.getRuntime().exec("clear");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
