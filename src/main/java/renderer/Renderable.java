package renderer;

public interface Renderable {
    public int getX();
    public int getY();
    public int getWidth();
    public int getHeight();
    public BitMap getBitmap();
}
