import blocks.Dirt;
import blocks.DirtTop;
import blocks.DirtTopLeft;
import blocks.DirtTopRight;
import camera.Camera;
import world.World;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        World world = new World(1024, 128);
        Camera camera = new Camera(20, 10);

        camera.offsetX = 5;
        camera.offsetY = 2;

        camera.setWorld(world);

        world.add(DirtTopLeft.build(8, 4));
        world.add(DirtTop.build(16, 4));
        world.add(DirtTopRight.build(24, 4));

        Scanner in = new Scanner(System.in);

        while (true) {

            camera.renderer.render(camera.watch());

            String command = in.next();

            if(command.equals("l")) {
                camera.offsetX--;
            }

            if(command.equals("r")) {
                camera.offsetX++;
            }

            if(command.equals("t")) {
                camera.offsetY--;
            }

            if(command.equals("b")) {
                camera.offsetY++;
            }

        }

    }
}
