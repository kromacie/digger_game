package world;

import camera.Camera;
import blocks.Block;
import renderer.Renderable;

import java.util.ArrayList;

public class World {
    private int width;
    private int height;

    private int block_width;
    private int block_height;

    private ArrayList<Renderable> blocks;

    public World(int width, int height) {
        this(width, height, 8, 4);
    }

    public World(int width, int height, int block_width, int block_height) {
        this.width = width;
        this.height = height;
        this.block_height = block_height;
        this.block_width = block_width;

        blocks = new ArrayList<>();
    }

    public void add(Renderable block) {
        this.blocks.add(block);
    }

    public void remove(Renderable block) {
        this.blocks.remove(block);
    }

    public ArrayList<Renderable> getBlocksInRange(int x, int y, int x_offset, int y_offset) {

        int x_finish = x + x_offset - 1;
        int y_finish = x + x_offset - 1;

        ArrayList<Renderable> chunk = new ArrayList<>();

        for (Renderable block: blocks) {

            int block_x_start = block.getX();
            int block_x_finish = block_x_start + block.getWidth() - 1;

            int block_y_start = block.getY();
            int block_y_finish = block_y_start + block.getHeight() - 1;

            boolean in_range_x = false;
            boolean in_range_y = false;

            if(block_x_start >= x && block_x_start <= x_finish || block_x_finish >= x && block_x_finish <= x_finish) {
                in_range_x = true;
            }

            if(block_y_start >= y && block_y_start <= y_finish || block_y_finish >= y && block_y_finish <= y_finish) {
                in_range_y = true;
            }

            if(in_range_x && in_range_y) {
                chunk.add(block);
            }

        }
        return chunk;
    }

}
